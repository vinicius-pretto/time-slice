import java.util.Scanner;

public class Application {
	private static Scanner scanner;

	public static void main(String args[]) {
		scanner = new Scanner(System.in);
		String processNames[] = { "p1", "p2", "p3", "p4" };
		int priorityOrders[] = { 0, 1, 2, 3 };
		int executionTimes[] = { 10, 4, 5, 3 };
		
		System.out.println("Time slice: ");
		int timeSlice = scanner.nextInt();
		RoundRobin.run(processNames, priorityOrders, executionTimes, timeSlice);
	}
}