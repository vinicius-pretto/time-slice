
class RoundRobin {
	static void run(String processNames[], int priorityOrders[], int executionTimes[], int timeSlice) {
		String TAB = "\t";
		int time = 0;
		int averageWaitingTime = 0;
		int averageCompletionTime = 0;
		int executionTimeResponse[] = new int[executionTimes.length]; // 4
		int priorityOrderResponse[] = new int[priorityOrders.length]; // 4
		int storageWaitingTime[] = new int[processNames.length];      // 4
		int storageCompletionTime[] = new int[processNames.length];   // 4
		String processSequence = new String();

		for (int i = 0; i < executionTimeResponse.length; i++) {
			executionTimeResponse[i] = executionTimes[i];
			priorityOrderResponse[i] = priorityOrders[i];
		}

		while (true) {
			boolean flag = true;

			for (int i = 0; i < processNames.length; i++) {
				if (priorityOrderResponse[i] <= time) {
					if (priorityOrderResponse[i] <= timeSlice) {
						if (executionTimeResponse[i] > 0) {
							flag = false;
							if (executionTimeResponse[i] > timeSlice) {
								time = time + timeSlice;
								executionTimeResponse[i] = executionTimeResponse[i] - timeSlice;
								priorityOrderResponse[i] = priorityOrderResponse[i] + timeSlice;
								processSequence += "->" + processNames[i];
							} else {
								time = time + executionTimeResponse[i];
								storageCompletionTime[i] = time - priorityOrders[i];
								storageWaitingTime[i] = time - executionTimes[i] - priorityOrders[i];
								executionTimeResponse[i] = 0;
								processSequence += "->" + processNames[i];
							}
						}
					} else if (priorityOrderResponse[i] > timeSlice) {
						for (int j = 0; j < processNames.length; j++) {
							if (priorityOrderResponse[j] < priorityOrderResponse[i]) {
								if (executionTimeResponse[j] > 0) {
									flag = false;
									if (executionTimeResponse[j] > timeSlice) {
										time = time + timeSlice;
										executionTimeResponse[j] = executionTimeResponse[j] - timeSlice;
										priorityOrderResponse[j] = priorityOrderResponse[j] + timeSlice;
										processSequence += "->" + processNames[j];
									} else {
										time = time + executionTimeResponse[j];
										storageCompletionTime[j] = time - priorityOrders[j];
										storageWaitingTime[j] = time - executionTimes[j] - priorityOrders[j];
										executionTimeResponse[j] = 0;
										processSequence += "->" + processNames[j];
									}
								}
							}
						}
						if (executionTimeResponse[i] > 0) {
							flag = false;

							if (executionTimeResponse[i] > timeSlice) {
								time = time + timeSlice;
								executionTimeResponse[i] = executionTimeResponse[i] - timeSlice;
								priorityOrderResponse[i] = priorityOrderResponse[i] + timeSlice;
								processSequence += "->" + processNames[i];
							} else {
								time = time + executionTimeResponse[i];
								storageCompletionTime[i] = time - priorityOrders[i];
								storageWaitingTime[i] = time - executionTimes[i] - priorityOrders[i];
								executionTimeResponse[i] = 0;
								processSequence += "->" + processNames[i];
							}
						}
					}
				} else if (priorityOrderResponse[i] > time) {
					time++;
					i--;
				}
			}
			if (flag) {
				break;
			}
		}
		System.out.println("Name" + TAB + "Completion time" + TAB + "Waiting time");

		for (int i = 0; i < processNames.length; i++) {
			System.out.println(processNames[i] + TAB + storageCompletionTime[i] + TAB + TAB + storageWaitingTime[i]);
			averageWaitingTime = averageWaitingTime + storageWaitingTime[i];
			averageCompletionTime = averageCompletionTime + storageCompletionTime[i];
		}
		System.out.println("\nAverage waiting time is " + (float) averageWaitingTime / processNames.length);
		System.out.println("Average completion time is " + (float) averageCompletionTime / processNames.length);
		System.out.println("Sequence is like that " + processSequence);
	}
}
